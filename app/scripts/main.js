/*!
 *
 *  Web Starter Kit
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */
/* eslint-env browser */
(function() {
  'use strict';

  // Check to make sure service workers are supported in the current browser,
  // and that the current page is accessed from a secure origin. Using a
  // service worker from an insecure origin will trigger JS console errors. See
  // http://www.chromium.org/Home/chromium-security/prefer-secure-origins-for-powerful-new-features
  var isLocalhost = Boolean(window.location.hostname === 'localhost' ||
    // [::1] is the IPv6 localhost address.
    window.location.hostname === '[::1]' ||
    // 127.0.0.1/8 is considered localhost for IPv4.
    window.location.hostname.match(
      /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
    )
  );

  if ('serviceWorker' in navigator &&
    (window.location.protocol === 'https:' || isLocalhost)) {
    navigator.serviceWorker.register('service-worker.js')
      .then(function(registration) {
        // updatefound is fired if service-worker.js changes.
        registration.onupdatefound = function() {
          // updatefound is also fired the very first time the SW is installed,
          // and there's no need to prompt for a reload at that point.
          // So check here to see if the page is already controlled,
          // i.e. whether there's an existing service worker.
          if (navigator.serviceWorker.controller) {
            // The updatefound event implies that registration.installing is set:
            // https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#service-worker-container-updatefound-event
            var installingWorker = registration.installing;

            installingWorker.onstatechange = function() {
              switch (installingWorker.state) {
                case 'installed':
                  $('#infoPop').find('.textBlock').text('Saved for offline use');
                  $('#infoPop').removeClass('error').removeClass('closed');
                  setTimeout(function() {
                    $('#infoPop').addClass('closed');
                  }, 2000);
                  break;

                case 'active':
                  $('#infoPop').find('.textBlock').text('Saved for offline use');
                  $('#infoPop').removeClass('error').removeClass('closed');
                  setTimeout(function() {
                    $('#infoPop').addClass('closed');
                  }, 2000);
                  break;

                case 'redundant':
                  throw new Error('The installing ' +
                    'service worker became redundant.');

                default:
                  break;
              }
            };
          }
        };
      }).catch(function(e) {
        console.error('Error during service worker registration:', e);
      });
  }

  // Your custom JavaScript goes here
  window.knowBill = window.knowBill || {};

  $.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
      if (o[this.name] !== undefined) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  window.knowBill = function() {
    return {
      init: function() {
        this.settoggle();

        this.setSubmitbutton();
      },

      settoggle: function() {
        var self = this;
        console.log = function(){};
        // var elem = document.querySelector('.jsSwitch');
        // self.switchToggle = new Switchery(elem, {
        //   color: '#E4E7EC',
        //   secondaryColor: '#E4E7EC',
        //   jackColor: '#263858',
        //   size: 'small'
        //     // color: '#263858',
        //     // secondaryColor: '#E4E7EC',
        //     // jackColor: '#FFF'
        // });

        var $alcToggle = $('#toggle');
        var $alEntry = $('#alcEntry');
        var $alcLabel = $('#alcLabel');

        // $alcLabel.on('click', function() {
        //   $alcToggle.trigger('click');
        // });
        var toggleValInit = $alcToggle.prop('checked');

        self.setAlcTextField(toggleValInit);

        $alcToggle.on('change', function() {
          var toggleVal = $(this).prop('checked');

          self.setAlcTextField(toggleVal)
        });
      },
      setAlcTextField: function(toggleVal){
        var $alEntry = $('#alcEntry');

        if (toggleVal) {
          $alEntry.removeClass('hide');
        } else {
          $alEntry.addClass('hide');
          $('[name="alcoholicBev"]').val("");
        }
      },

      setSubmitbutton: function() {
        var ua = navigator.userAgent.toLowerCase();
        var self = this;

        var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
        if (!isAndroid) {

          $('.wrapper').removeClass('isAndroid');
        }

        $('#btnSubmit').on('click', function(e) {
          e.preventDefault();
          self.billCheck();
        });

        $('#billInputForm').find('input').on('change', function() {
          self.btnChangeClr();
        });

        $('#back').on('click', function() {
          $('.wrapper').removeClass('billSplit');
        });

        $('#faq').on('click', function() {
          $('#overlay').show();
          $('#infoModal').fadeIn();
        });

        $('#closeModal').on('click', function() {
          $('#overlay').hide();
          $('#infoModal').fadeOut();
        });
      },
      billCheck: function(checkVal) {
        //$('.wrapper').addClass('billSplit');
        var self = this;
        var alcSwitchChecked = $('#toggle').prop('checked');

        if (alcSwitchChecked) {

        } else {
          $('[name="alcoholicBev"]').val("");
        }

        var billVals = $('#billInputForm').serializeObject();




        if (alcSwitchChecked && (billVals.alcoholicBev == "" || billVals.alcoholicBev == "0" ) && (billVals.nonAlcoholic == "" || billVals.nonAlcoholic == "0")) {
          self.showError('Please enter valid amount.');
        } else if (!alcSwitchChecked && (billVals.nonAlcoholic == "" || billVals.nonAlcoholic == "0")) {
          self.showError('Please enter valid amount.');
        } else {

          self.billCalculate(billVals);
        }

      },
      btnChangeClr: function() {
        var self = this;
        var billVals = $('#billInputForm').serializeObject();
        var alcSwitchChecked = $('#toggle').prop('checked');


        if (alcSwitchChecked && (billVals.alcoholicBev == "" && billVals.nonAlcoholic == "") || (billVals.alcoholicBev == "0" && billVals.nonAlcoholic == "0")) {
        } else if (!alcSwitchChecked && billVals.nonAlcoholic == "" || billVals.nonAlcoholic == "0  ") {
        } else {

          $('#btnSubmit').addClass('go');
        }

      },
      showError: function(textVal) {
        $('#infoPop').find('.textBlock').text(textVal);
        $('#infoPop').addClass('error').removeClass('closed');
        setTimeout(function() {
          $('#infoPop').addClass('closed');
        }, 2000);
      },
      billCalculate: function(billVals) {
        console.log(billVals);

        var self = this;

        var convObj = {};

        $.each(billVals, function(key, value) {
          console.log(key, value);
          if (value) {
            convObj[key] = parseFloat(value);
          } else {
            convObj[key] = 0;
          }

        });

        console.log(convObj);

        $('#scharge').html(convObj.serviceChg);
        $('#scharge').next('.info').attr('data-balloon', convObj.serviceChg+'% of Sub Total')

        if(convObj.alcoholicBev){
          var totalTip = "Alcohol ( "+(convObj.alcoholicBev).toFixed(2)+" ) + Food ( "+(convObj.nonAlcoholic).toFixed(2) +" )";
          $('#sTotal').find('.info').attr('data-balloon', totalTip);
        } else {
          var totalTip = "Food ( "+(convObj.nonAlcoholic).toFixed(2) +" ) ";
          $('#sTotal').find('.info').attr('data-balloon', totalTip);
        }



        var resultObj = {};

        if (convObj.serviceChg) {
          var serviceCharge = ((convObj.alcoholicBev + convObj.nonAlcoholic) / 100) * convObj.serviceChg;
          resultObj['serviceChg'] = (serviceCharge).toFixed(2);
          $('#scItem').show();
        } else {
          resultObj['serviceChg'] = "0.00";
          $('#scItem').hide();
        }

        if (convObj.alcoholicBev) {
          var vatAlc = (convObj.alcoholicBev / 100) * 5.5;
          resultObj['vatAlc'] = (vatAlc).toFixed(2);
          $('#vatAlcItem').show();
        } else {
          $('#vatAlcItem').hide();
        }

        var vatOther = (convObj.nonAlcoholic / 100) * 14.5;
        resultObj['vatOther'] = (vatOther).toFixed(2);

        var serviceChargeNum = parseFloat(resultObj['serviceChg']);

        var serviceTax = ((convObj.alcoholicBev + convObj.nonAlcoholic + serviceChargeNum) / 100) * 6;
        resultObj['serviceTax'] = (serviceTax).toFixed(2);

        console.log(resultObj);

        var total = 0;

        $.each(resultObj, function(key, value) {
          console.log(key, value);
          $('#' + key).html('&#8377; '+ value);
          total = total + parseFloat(value, 10);
        });

        total = total + convObj.alcoholicBev + convObj.nonAlcoholic;



        total = (total).toFixed(2);

        console.log(total);

        var grossTotal = (convObj.alcoholicBev + convObj.nonAlcoholic).toFixed(2);

        $('#grossTotal').html('&#8377; '+ grossTotal);
        $('#total').html('&#8377; '+total);

        $('.wrapper').addClass('billSplit');

        $('.info').on('click', function(){
          $('.info').removeClass('hover');
          $(this).addClass('hover');
          self.closeTip();

        });


      },
      closeTip : function(){
        var self = this;
        window.clearTimeout(self.popTimeout);
        self.popTimeout = setTimeout(function(){
            $('.info').removeClass('hover');
        }, 2000);
      }

    }
  }

  window.onload = function() {
    window.knowBill().init();
  }


})();
