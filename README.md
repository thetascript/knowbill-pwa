## KnowBill

### Bill Analysis

This app will give the users the ability to know their restaurant bill splits (VAT, Service Tax ., etc) in a clear and concise way.

#### Requirements

[https://paper.dropbox.com/doc/KnowBill-Requirements-y3R9FXBWCzlVLh04IbScL](https://paper.dropbox.com/doc/KnowBill-Requirements-y3R9FXBWCzlVLh04IbScL)

#### installation

Follow [installation docs](docs/install.md)

then on the project folder run

`` gulp serve `` or ``gulp serve:dist`` (prod build)
